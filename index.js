const fs = require('fs');
const assert = require('assert');
const dgram = require('dgram');
const commander = require('commander'); // https://github.com/tj/commander.js/

let gNextId = 0xff000000;

function createSocket() {
  let sock = dgram.createSocket('udp4');
  sock.ownerId = gNextId++;
  sock.sendPretty = function (remote, message, toCpp) {
    if (typeof remote === 'string') remote = { address: remote.split(':')[0], port: remote.split(':')[1] };
    if (!message) message = '' + message;
    else if (typeof message !== 'string') message = JSON.stringify(message);
    if (toCpp) message += '\000';
    let buf = Buffer.from(message, 'utf8');
    let buf2 = Buffer.alloc(16);
    buf2.writeUInt32LE(0, 0);
    buf2.writeUInt32LE(0x3c, 4);
    buf2.writeUInt32LE(0xFF000000, 8);
    buf2.writeUInt32LE(0xFF000000, 12);
    message = Buffer.concat([buf2, buf]);
    sock.send(message, 0, message.length, remote.port, remote.address, () => {});
  };
  return sock;
}

// Возвращает случайное целое число между min (включительно) и max (не включая max)
// Использование метода Math.round() даст вам неравномерное распределение!
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function createMessage(ownerId) {
  return {
    movement: { pos: [1,1,1], rot: [0,0,0] },
    cellOrWorld: 0x3c,
    ownerId,
    timestamp: Date.now(),
    rand: getRandomInt(0, 2000000000),
    token: 'abc',
    myId: ownerId
  };
}

function createTestId() {
  return Math.random() + '' + Math.random() + '' + Math.random();
}

const package = JSON.parse(fs.readFileSync('./package.json'));
commander
.version(package.version)
.option('-t, --target [address]', 'Cellhost ip and port')
.option('-c, --count [integer]', 'Count of sockets')
.parse(process.argv);

let { target } = commander;
const NUM_SOCKETS = commander.count;
const SEND_TO_INTERVAL_MS = 200;

let socks = [];
let gCache = {};
let gLastPrint;

let totalMessages = 0;
let totalDelay = 0;

for (let i = 0; i < NUM_SOCKETS; ++i) {
  let sock = createSocket();
  sock.on('message', (message, remote) => {
    message = message.toString().split('');
    //message.pop(); // null character
    message = message.join('');
    message = JSON.parse(message);
    if (1) {
      totalMessages++;
      totalDelay += Date.now() - message.timestamp;
      if (!gLastPrint || Date.now() - gLastPrint > 100) {
        console.log(totalDelay / totalMessages);
        gLastPrint = Date.now();
      }
    }
    //if (!gCache[message.testId]) gCache[message.testId] = 0;
    //++gCache[message.testId];
  });
  socks.push(sock);
}

setInterval(() => {
  socks.forEach(sock => {
    let msg = createMessage(sock.ownerId);
    msg.testId = createTestId();
    sock.sendPretty(target, msg);
  })
}, SEND_TO_INTERVAL_MS);

/*setInterval(() => {
  let was = Date.now();
  let sum = 0;
  let count = 0;
  for (let prop in gCache) {
    count++;
    sum += gCache[prop];
  }
  let res = sum / count;
  console.log(res, 'calulated in', Date.now() - was, 'ms');
}, 1000);
*/
